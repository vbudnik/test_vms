import os
import sys

def main():

	step = 10
	mode = 'test'
	n = 0

	print("Use python make_all_test.py [name_file] [step] [mode_start 't' -- for testing, 's' -- for start all procces]")

	if len(sys.argv) == 4:
		mode = sys.argv[3]
		step = int(sys.argv[2])
		file = open(sys.argv[1], "r")
		if file.mode == 'r':
			contents = file.read()
		quiz = contents.split('\n')
		n = len(quiz) + 1

	i = step
	run_string = []
	while i <= n:
		if i == 0:
			run_string.append('python test_vms_v3.py file_for_check_fail_result 0 ' + str(i) + ' > result_final/test_0_' + str(i) + ' &')
		if (i + step) > n:
			run_string.append('python test_vms_v3.py file_for_check_fail_result ' + str(i - step) + ' ' + str(n) + ' > result_final/test_' + str(i - step) + '_' + str(n) + ' &')
			break
		else:
			run_string.append('python test_vms_v3.py file_for_check_fail_result ' + str(i - step) + ' ' + str(i) + ' > result_final/test_' + str(i - step) + '_' + str(i) + ' &')
		i = i + step

	for i in range(len(run_string)):
		if mode == 's':
			os.system(run_string[i])
		if mode == 't':
			print(run_string[i])
	exit()
	
if __name__ == "__main__":
	main()