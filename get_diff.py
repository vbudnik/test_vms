import os
import sys
import itertools

def main():

	def make_combination(quiz_list, len_quiz):
		first_list = list()
		list_of_list = list()
		for i in range(len_quiz):
			if i == len_quiz - 1:
				list_of_list.append((list(first_list)))
				del first_list[:]
				first_list.append(quiz_list[i])
			if i == 0:
				first_list.append(quiz_list[i])
				continue
			if quiz_list[i].split('_')[0] == quiz_list[i - 1].split('_')[0]:
				first_list.append(quiz_list[i])
			else:
				list_of_list.append((list(first_list)))
				del first_list[:]
				first_list.append(quiz_list[i])
		res = list(itertools.product(*list_of_list))

		return res

	def make_diff(booster, pack, quiz):

		if (quiz.lower()).find((booster.lower())) != -1 and (quiz.lower()).find((pack.lower())) != -1:
			return True
		return False 

	# get masater file
	file = open(sys.argv[2], "r")
	if file.mode == 'r':
		contents = file.read()
	master = contents.split('\n')


	len_quiz = len(master)
	quiz_list = list()

	for i in range(len_quiz):
		quiz_list.append(master[i].split(', '))

	master_combination = list()
	fin_len = len(quiz_list)
	for i in range(fin_len):
		master_combination.append(make_combination(quiz_list[i], len(quiz_list[i])))


	# get result file
	file = open(sys.argv[1], "r")
	if file.mode == 'r':
		contents = file.read()
	quiz = contents.split('\n\n\n')
	
	i = 0
	q = {}
	while i < len(quiz):
		q[i] = quiz[i].split('\n')
		i = i + 1

	for j in range(len(master_combination)):
		if j == 80:
			print(j)
			for i in range(len(master_combination[j])):
				for k in range(len(q)):
					# if k < 10:
					# 	print("K = " + str(k))
					if str(master_combination[j][i]).split('PACK: ')[0] == (q[k][len(q[k]) - 4]).split('PACK: ')[0]:
						# print(k)
						# print(quiz[k])
						booster = ((str(master_combination[j][i]).split('PACK: ')[1]).replace(')', "").replace('\'', "").replace("BOOSTER: ", "")).split(', ')[1]
						pack = ((str(master_combination[j][i]).split('PACK: ')[1]).replace(')', "").replace('\'', "").replace("BOOSTER: ", "")).split(', ')[0]
						if (q[k][len(q[k]) - 4]).split('PACK: ')[1] == str(master_combination[j][i]).split('PACK: ')[1]:
							# print(str(master_combination[j][i]) + "FAIL!!!")
							# exit()
							break
							pass
						else:
							if make_diff(booster, pack, quiz[k]):
								# print(str(master_combination[j][i]) + "DONE!!!")
								# print(quiz[k])
								pass
							else:
								print(str(master_combination[j][i]) + "FAIL!!!")
								print(quiz[k])

					else:
						booster = ((str(master_combination[j][i]).split('PACK: ')[1]).replace(')', "").replace('\'', "").replace("BOOSTER: ", "")).split(', ')[1]
						pack = ((str(master_combination[j][i]).split('PACK: ')[1]).replace(')', "").replace('\'', "").replace("BOOSTER: ", "")).split(', ')[0]
						if make_diff(booster, pack, quiz[k]):
							print(str(master_combination[j][i]) + "DONE!!!")
							print(quiz[k])
							exit()
						else:
							# print(str(master_combination[j][i]) + "FAIL!!!")
							# print(quiz[k])
							pass






	exit()
	
if __name__ == "__main__":
	main()