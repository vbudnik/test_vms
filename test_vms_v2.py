# encoding: utf-8

import itertools
import platform
import time
import datetime
import sys
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import \
    			NoSuchElementException, \
    				WebDriverException


def main():
	const_quize = {
		'2' : {
				'q':'How young are you?',
				'name_class': 'quiz-item-name-1-4',
				},
		'4' : {
				'q':'What gender do you most closely identify with?',
				'name_class': 'quiz-item-name-1-6',
				},
		'5' : {
				'q':'Are you...?',
				'name_class': 'quiz-item-name-1-7',
				},
		'6' : {
				'q':'Are you allergic to any of the following?  Check all that apply:',
				'name_class': 'quiz-item-name-1-9',
				},

		'7' : {
				'q':'Do you have any special dietary needs? Check all that apply.',
				'name_class': 'quiz-item-name-1-10',
				},
		'8_1' : {
				'q':'How many servings PER DAY do you typically eat of the following: FRUIT',
				'name_class': 'quiz-item-name-1-11',
				},
		'8_2' : {
				'q':'How many servings PER DAY do you typically eat of the following: VEGETABLES',
				'name_class': 'quiz-item-name-1-12',
				},
		'8_3' : {
				'q':'How many servings PER DAY do you typically eat of the following: GRAINS',
				'name_class': 'quiz-item-name-1-13',
				},
		'8_4' : {
				'q':'How many servings PER DAY do you typically eat of the following: WHOLE GRAINS',
				'name_class': 'quiz-item-name-1-14',
				},
		'8_5' : {
				'q':'How many servings PER DAY do you typically eat of the following: DAIRY',
				'name_class': 'quiz-item-name-1-15',
				},
		'8_6' : {
				'q':'How many servings PER DAY do you typically eat of the following: MEAT,FISH,CHICKEN,EGGS',
				'name_class': 'quiz-item-name-1-16',
				},
		'8_7' : {
				'q':'How many servings PER DAY do you typically eat of the following: BEANS OR LENTILS',
				'name_class': 'quiz-item-name-1-17',
				},
		'9' : {
				'q':'When it comes to food...',
				'name_class': 'quiz-item-name-1-18',
				},
		'10' : {
				'q':'How much water and other NON-alcoholic beverages do you drink on a typical day?',
				'name_class': 'quiz-item-name-1-19',
				},
		'11' : {
				'q':'On average, how many hours of sleep do you get at night?',
				'name_class':'quiz-item-name-1-21',
				},
		'12' : {
				'q':'When you wake up in the morning, are you usually refreshed?',
				'name_class': 'quiz-item-name-1-22',
				},
		'13' : {
				'q':'Tell us about your energy levels:',
				'name_class': 'quiz-item-name-1-23',
				},
		'14' : {
				'q':'How often do you exercise?',
				'name_class': 'quiz-item-name-1-24',
				},
		'15' : {
				'q':'How does physical activity fit into your life?',
				'name_class': 'quiz-item-name-1-25',
				},
		'16' : {
				'q':'In a typical day, I...',
				'name_class': 'quiz-item-name-1-26',
				},
		'17' : {
				'q':'When I travel, I feel...',
				'name_class': 'quiz-item-name-1-27',
				},
		'18' : {
				'q':'Thinking about your health goals, what is your priority?',
				'name_class': 'quiz-item-name-1-29',
				},
		'19' : {
				'q':'Now, let\'s think about your digestive health.',
				'name_class': 'quiz-item-name-1-30',
				},
		'20' : {
				'q':'Thinking about how stressed you\'ve been over the last few weeks...',
				'name_class': 'quiz-item-name-1-31',
				},
		'21' : {
				'q':'Do you currently use gummy vitamins?',
				'name_class': 'quiz-item-name-1-32',
				},
		'22' : {
				'q':'Are you currently taking a multivitamin?',
				'name_class': 'quiz-item-name-1-33',
				},
	}

	q8 = ('quiz-item-name-1-11', 'quiz-item-name-1-12', 'quiz-item-name-1-13', 'quiz-item-name-1-14',
			'quiz-item-name-1-15', 'quiz-item-name-1-16', 'quiz-item-name-1-17')


	def replace_char(text):
		return text.replace(u'\u2019', "'").replace(u'\u2022', " ").replace(u'\u2013', "-").replace(u'\u2026', "...")

	def check_current_url():
		if driver.current_url == 'https://vms.huntersconsult.com/survey/index/result/' or driver.current_url == 'https://vms.huntersconsult.com/survey-result':
			return True
		return False

	def get_question():
		if check_current_url():
			return
		try:
			time.sleep(time_sleep)
			result = driver.find_element_by_xpath("//div[@class='value']")
			return replace_char(result.text)
		except (NoSuchElementException, AssertionError, WebDriverException):
			result = driver.find_element_by_xpath("//div[@class='text js-text']")
			return replace_char(result.text)

	def regist():
		try:
			driver.find_element_by_link_text("Next").click()

			time.sleep(time_sleep)
			name = driver.find_element_by_name('quiz-item-name-1-2')
			name.send_keys('a')
			driver.find_element_by_link_text("Next").click()

			time.sleep(time_sleep)
			email = driver.find_element_by_name('quiz-item-name-1-3')
			email.send_keys('a@a.com')
			driver.find_element_by_link_text("Next").click()
			time.sleep(time_sleep)
			state = driver.find_element_by_xpath("//select[@name='quiz-item-name-1-44']/option[text()='Alaska']").click()
			driver.find_element_by_link_text("Next").click()

		except (NoSuchElementException, AssertionError, WebDriverException):
			print("Problem with registration");
			driver.get('https://vms.huntersconsult.com/survey/')
			regist()

	def fin_question():
		try:
			if check_current_url():
				return
			get_question()
			element = driver.find_element_by_xpath("//input[@class='input-radio']").get_attribute('name')
			js_string = "document.getElementsByName('" + element + "')[1].click();"
			check_box = driver.execute_script(js_string)
			search_box = driver.find_element_by_link_text("Next")
			search_box.click()
			fin_question()
		except (NoSuchElementException, AssertionError, WebDriverException):
			try:
				if check_current_url():
					return
				element = driver.find_element_by_xpath("//input[@class='input-radio']").get_attribute('name')
				search_box = driver.find_element_by_link_text("Next")
				search_box.click()
				fin_question()
			except (NoSuchElementException, AssertionError, WebDriverException):
				search_box = driver.find_element_by_link_text("Next")
				search_box.click()
				fin_question()


	def make_q8(quiz):
		try:
			if check_current_url() == True:
				return
			q = quiz.split('_')
			value = 0
			if q[1] == 'HIGH':
				value = 4
			else:
				value = 1
			now = get_question()

			for const_quizes in const_quize:
				if const_quize[const_quizes]['q'].find(now.split('\n')[0]) != -1:
					element = const_quize[const_quizes]['name_class'];

			element = driver.find_element_by_xpath("//input[@class='input-radio']").get_attribute('name')
			len_q8 = len(q8)
			flag = False
			for i in range(len_q8):
				if q8[i] == element:
					flag = True
					break 
			if flag == False:
				return
			js_string = "document.getElementsByName('"+element+"')["+ str(value) +"].click();"

			check_box = driver.execute_script(js_string)
			search_box = driver.find_element_by_link_text("Next")
			search_box.click()
			make_q8(quiz)
		except (NoSuchElementException, AssertionError, WebDriverException):
			search_box = driver.find_element_by_link_text("Next")
			search_box.click()
			make_q8(quiz)

	def makeAction(quiz):
		if quiz == '8_HIGH' or quiz == '8_LOW':
			make_q8(quiz)
			return
		try:
			if check_current_url() == True:
				return False

			now = get_question()
			q = quiz.split('_')

			element = ''
			for const_quizes in const_quize:
				if const_quize[const_quizes]['q'].find(replace_char(now.split('\n')[0])) != -1:
					element = const_quize[const_quizes]['name_class']
					if const_quizes != q[0]:
						try:
							element = driver.find_element_by_xpath("//input[@class='input-radio']").get_attribute('name')
							js_string = "document.getElementsByName('"+element+"')[1].click();"
							check_box = driver.execute_script(js_string)
							search_box = driver.find_element_by_link_text("Next")
							search_box.click()
							makeAction(quiz)
						except (NoSuchElementException, AssertionError, WebDriverException):
							return False

			answer = str(int(q[1]) - 1)
			js_string = "document.getElementsByName('"+element+"')["+answer+"].click();"
			check_box = driver.execute_script(js_string)
			search_box = driver.find_element_by_link_text("Next")
			search_box.click()

		except (NoSuchElementException, AssertionError, WebDriverException):
			try:
				if check_current_url() == True:
					return
				element = driver.find_element_by_xpath("//input[@class='input-radio']").get_attribute('name')
				js_string = "document.getElementsByName('"+element+"')[1].click();"
				check_box = driver.execute_script(js_string)
				search_box = driver.find_element_by_link_text("Next")
				search_box.click()
				return 
			except (NoSuchElementException, AssertionError, WebDriverException):
				if check_current_url() == True:
					return False


	""" make test """
	def test_vms(quize):

		# make registration
		regist()
		len_q = len(quize)
		flag = True
		# make test for all element in question
		for i in range(len_q):
			if check_current_url() == True:
				break
			if quize[i].find('PACK:') >= 0 or quize[i].find('BOOSTER:') >= 0:
				break
			if i >= 0:
				# Make some action for if rothe not exitst, Now, I return False
				exept = driver.find_element_by_xpath("//div[@class='text js-text']")
				if exept.text:
					next_button = driver.find_element_by_link_text("Next")
					next_button.click()
				result = makeAction(quize[i])
				if result == False:
					flag = False
					break
		
		# quize not finishet make final quize

		fin_question()

		# get result page
		try:
			booster = driver.find_element_by_xpath("//div[@class='product-name product-name-desktop']")
			pack = driver.find_element_by_xpath("//p[@class='description']")

			origin_pack = quize[len_q - 2].replace("PACK: ", "")
			origin_booster = quize[len_q - 1].replace("BOOSTER: ", "")
			page_pack = replace_char(pack.text)
			page_booster = replace_char(booster.text)

			if page_pack.find(origin_pack) != -1 and page_booster.find(origin_booster) != -1:
				print("PACK: " + origin_pack + "\nBOOSTER: " + origin_booster)
				# driver.save_screenshot("./screanchot_secon/succses/"+ str(quize) +".png")
				print("DONE !!!!")
			else:
				print("PACK: " + page_pack + "\nBOOSTER: " + page_booster)
				# driver.save_screenshot("./screanchot_secon/fail/"+ str(quize) +".png")
				print("ORIGIN BOOSTER: " + origin_booster)
				print("ORIGIN PACK: " + origin_pack)
				print(quize)
				print("FAIL !!!!")


		except (NoSuchElementException, AssertionError, WebDriverException):
			print("FAIL !!!!")
			print(driver.current_url)
			# driver.save_screenshot("./screanchot_secon/fail/"+ str(quize) +".png")
			print(quize)
			print('')
			return 

	def make_combination(quiz_list, len_quiz):
		first_list = list()
		list_of_list = list()
		for i in range(len_quiz):
			if i == len_quiz - 1:
				list_of_list.append((list(first_list)))
				del first_list[:]
				first_list.append(quiz_list[i])
			if i == 0:
				first_list.append(quiz_list[i])
				continue
			if quiz_list[i].split('_')[0] == quiz_list[i - 1].split('_')[0]:
				first_list.append(quiz_list[i])
			else:
				list_of_list.append((list(first_list)))
				del first_list[:]
				first_list.append(quiz_list[i])
		res = list(itertools.product(*list_of_list))

		return res


	""" create window in background"""
	chrome_options = webdriver.ChromeOptions()
	chrome_options.add_argument('--headless')
	chrome_options.add_argument('--no-sandbox')
	driver = webdriver.Chrome('/home/vbudnik/silenium/chromedriver_linux64/chromedriver', chrome_options=chrome_options)

	# if platform.node() == 'Yaroslavl':
	#  	""" create window chrome"""
	#  	driver = webdriver.Chrome('/home/vbudnik/silenium/chromedriver_linux64/chromedriver')
	#  	driver.get('https://vms.huntersconsult.com/survey/')
	# else:
	#  	""" for fire fox """
	#  	driver = webdriver.Firefox(executable_path='/home/vbudnik/silenium/geckodriver')
	#  	driver.get('https://vms.huntersconsult.com/survey/')

	""" time sleep """
	time_sleep = 2

	""" read file with quize """

	file = open(sys.argv[1], "r")


	if file.mode == 'r':
		contents = file.read()
	quiz = contents.split('\n')

	len_quiz = len(quiz)
	quiz_list = list()

	for i in range(len_quiz):
		quiz_list.append(quiz[i].split(', '))

	start = 0
	end = len(quiz_list)
	if len(sys.argv) == 4:
		start = int(sys.argv[2])
		end = int(sys.argv[3])

	print("Start routh: " + str(start) + ", End routh: " + str(end))
	print("START TESTING AT:" + str(datetime.datetime.now()))
	
	for j in range(len(quiz_list)):
		if j >= start:
			if j == end:
				break
			start_time = datetime.datetime.now()
			print
			print("-------------- START TEST ---------------------------")
			print("Make test "+ str(j) + " for " + str(len(quiz_list)) + " tests;")
			print("Testing route:" + str(quiz_list[j]) + ";")
			
			test_vms(quiz_list[j])
			driver.get('https://vms.huntersconsult.com/survey/')
			time_test = datetime.datetime.now() - start_time
			
			print("Test time: "+ str(time_test)+";")
			print("-------------- FINSH TEST ---------------------------")
			print

	print("FINISH TESTING AT:" + str(datetime.datetime.now()))
	driver.quit()


if __name__ == "__main__":
	main()
