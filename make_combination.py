import itertools

quiz = "7_1, 7_4, 7_5, 7_6, 7_7, 7_10, 4_2, 2_1, 2_2, 5_4, 6_4, 6_6, 18_1, 19_1, 19_2, 8_LOW, PACK: DAILY FORTIFIER, BOOSTER: GUT CHECK"
quiz_list = quiz.split(", ")
len_quiz = len(quiz_list)


def make_combination(quiz_list, len_quiz):
	first_list = list()
	list_of_list = list()
	for i in range(len_quiz):
		if i == len_quiz - 1:
			list_of_list.append((list(first_list)))
			del first_list[:]
			first_list.append(quiz_list[i])
		if i == 0:
			first_list.append(quiz_list[i])
			continue
		if quiz_list[i].split('_')[0] == quiz_list[i - 1].split('_')[0]:
			first_list.append(quiz_list[i])
		else:
			list_of_list.append((list(first_list)))
			del first_list[:]
			first_list.append(quiz_list[i])

	res = list(itertools.product(*list_of_list))

	return res

print(len(make_combination(quiz_list, len_quiz)))